from django.shortcuts import render, redirect
from .models import Member, Activity
from .forms import MemberForm, ActivityForm
from django.contrib import messages

# Create your views here.

def activity(request):
    if request.method == "POST":
        activityForm = ActivityForm(request.POST)
        memberForm = MemberForm(request.POST)
        if activityForm.is_valid():
            activityForm.save()
            messages.success(request, (f"The activity \"{request.POST['activity']}\" has been successfully added!"))
            return redirect('story6:activity')
        elif memberForm.is_valid():
            memberForm.save()
            messages.success(request, (f"{request.POST['member']} successfully joined as a member!"))
            return redirect('story6:activity')
        else:
            messages.warning(request, (f"Incorrect input!"))
            return redirect('story6:activity')

    else:
        activityForm = ActivityForm()
        memberForm = MemberForm()
        activities = Activity.objects.all()
        members = Member.objects.all()
        context = {
            'activityForm' : activityForm,
            'memberForm' : memberForm,
            'activities' : activities,
            'members' : members
        }
        return render(request, 'story6/index.html', context)

def delete(request, pk):
    activity = Activity.objects.get(id=pk)
    activity.delete()
    messages.warning(request, (f"The activity \"{activity}\" has been successfully deleted!"))
    return redirect('story6:activity')
