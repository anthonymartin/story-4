from django.contrib import admin
from .models import Activity, Member

# Register your models here.
admin.site.register(Activity)
admin.site.register(Member)