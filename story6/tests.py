from django.test import TestCase, Client
from django.urls import reverse, resolve
from .models import Activity, Member

# Create your tests here.
class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.activity_url = reverse("story6:activity")
        self.test_activity = Activity.objects.create(activity="makan")
        self.test_member = Member.objects.create(member="MARTOUT", kegiatan=self.test_activity)
        self.test_delete_url = reverse("story6:activityDelete", args=[self.test_activity.id])

    def test_activity_GET(self):
        response = self.client.get(self.activity_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "story6/index.html")

    def test_activity_POST(self):
        response = self.client.post(self.activity_url, {"activity":"tidur"}, follow=True)
        self.assertContains(response, "tidur")

    def test_member_POST(self):
        response = self.client.post(self.activity_url, {"member":"MARTOUT", "kegiatan":self.test_activity.id,}, follow=True)
        self.assertContains(response, "MARTOUT")

    def test_invalid_POST(self):
        response = self.client.post(self.activity_url, {"member":"MARTOUT", "kegiatan":"lho kok beda",}, follow=True)
        self.assertContains(response, "Incorrect input")

    def test_activity_DELETE(self):
        response = self.client.get(self.test_delete_url, follow=True)
        self.assertContains(response, "successfully deleted")