from django.shortcuts import render

# Create your views here.
def homepage(request):
    return render(request, 'story4/index.html')

def profile(request):
    return render(request, 'story4/profile.html')

def gallery(request):
    return render(request, 'story4/gallery.html')

def debut_story(request):
    return render(request, 'story4/story1.html')