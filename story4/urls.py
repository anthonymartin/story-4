from django.urls import path
from . import views

app_name = 'story4'

urlpatterns = [
    path('', views.homepage, name='homepage'),
    path('profile/', views.profile, name='profile'),
    path('gallery/', views.gallery, name='gallery'),
    path('debut-story/', views.debut_story, name='debut_story'),
]