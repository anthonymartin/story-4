# STORY 4–6

Nama            : Anthony Martin Hasurungan <br>
NPM             : 1806185355 <br>
Kelas           : Perancangan dan Pemrograman Web / PPW A <br>
Link Website    : [https://martout-4.herokuapp.com/](https://martout-4.herokuapp.com/)


[![pipeline status](https://gitlab.com/anthonymartin/story-4/badges/master/pipeline.svg)](https://gitlab.com/anthonymartin/story-4/-/commits/master)

[![coverage report](https://gitlab.com/anthonymartin/story-4/badges/master/coverage.svg)](https://gitlab.com/anthonymartin/story-4/-/commits/master)
