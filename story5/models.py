from django.db import models

# Create your models here.

class Course(models.Model):
    TERM_CHOICES = [
        ('Ganjil', 'Ganjil'),
        ('Genap', 'Genap')
    ]
    YEAR_CHOICES = [
        ('2019/2020', '2019/2020'),
        ('2020/2021', '2020/2021'),
        ('2021/2022', '2021/2022'),
        ('2022/2023', '2022/2023'),
    ]
    name = models.CharField(max_length=50)
    teacher = models.CharField(max_length=100)
    sks = models.PositiveIntegerField()
    term = models.CharField(
        max_length=6,
        choices=TERM_CHOICES,
        default='Ganjil'
    )
    year = models.CharField(
        max_length=9,
        choices=YEAR_CHOICES,
        default='2019/2020'
    )
    room = models.CharField(max_length=20)
    desc = models.CharField(max_length=300)

    def __str__(self):
        return self.name
