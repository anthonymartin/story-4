from django import forms
from .models import Course

class CourseForm(forms.ModelForm):
    class Meta:
        model = Course
        fields = ['name', 'teacher', 'sks', 'term', 'year', 'room', 'desc']
        widgets = {
            'name' : forms.TextInput(attrs={'class':'form-control'}),
            'teacher' : forms.TextInput(attrs={'class':'form-control'}),
            'sks' : forms.NumberInput(attrs={'class':'form-control'}),
            'term' : forms.Select(attrs={'class':'form-control'}),
            'year' : forms.Select(attrs={'class':'form-control'}),
            'room' : forms.TextInput(attrs={'class':'form-control'}),
            'desc' : forms.TextInput(attrs={'class':'form-control'}),
        }