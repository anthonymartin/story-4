from django.shortcuts import render, redirect
from django.contrib import messages
from .models import Course
from .forms import CourseForm

# Create your views here.

def schedule(request):
    courses = Course.objects.all()
    context = {'courses' : courses}
    return render(request, 'story5/index.html', context)

def add(request):
    form = CourseForm()
    if request.method == "POST":
        form = CourseForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, (f"The course \"{request.POST['name']}\" has been successfully added to the list!"))
            return redirect('story5:schedule')
        else:
            messages.warning(request, ("The data you entered didn't match the criteria!"))
            return redirect('story5:add')
    context = {'form' : form}
    return render(request, 'story5/add.html', context)

def deleteItem(request, pk):
    course = Course.objects.get(id=pk)
    course.delete()
    messages.success(request, (f"The course \"{course}\" has been removed from the list."))
    return redirect('story5:schedule')
