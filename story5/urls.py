from django.urls import path
from . import views

app_name = 'story5'

urlpatterns = [
    path('', views.schedule, name='schedule'),
    path('add/', views.add, name='add'),
    path('delete/<str:pk>', views.deleteItem, name='delete'),
]
